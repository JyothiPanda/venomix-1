Venomix has been developed by [Jason Macrander](https://eeob.osu.edu/people/macrander.1) and Ed Rice while at The Ohio State University. Venomix is a programming suite desiged to make it easier to interpret and understand EST and Transcriptome data when looking for venom-like sequences. The program relies on inputs from the user (see below) that are common precursors to similar toxin gene identification processes.

##Updates
Version 0.7 - (Current) in prep verson

# Overview of Venomix

Venomix uses a suite of python and R scripts to combine data provided by the user to quickly screen transcriptomes for toxin or venom-like sequences. There are additional options for identifying signaling region and conserved domains (ie. cysteine residues), but this requries the installation of optional programs (see below).

A curated database of both toxin and venom genes can can be found on the [ToxProt](http://www.uniprot.org/program/Toxins) website. 

## Anticipated outcomes of Venomix:

Venomix follows the same path that other "venomics" approahces have utilized when collecting and understanding next-generation sequences. Using previously characterized toxin and venom protein sequences as the search query in blast, gene toxin genes can be identified using this pipeline. Although this program DOES NOT identify toxin/venom genes (that requires a bit more work) it can quickly tell you what you've found, what's the expression level, and what gene family the toxin-like sequence may belong to. 

Please Note: When you are working with an animal that is closely related to other species that have been studied this pipeline produces a high probability of toxin/venom genes. If you are working on a poorly studied group (like the cnidarians) please use it with caution.

The end result will produce three products:

* Fasta files with candidate toxin genes grouped by venom sequence similarty (inferred function similarity).
* A toxin-like gene summary that includes relevant information for the different toxin groups identified in your data set.
* A toxin gene report that compiles information from the previously studied proteins to get you started on your venomics investigation.

## Data Requirements

Your sequence data set must be in FASTA format:

    >Sequence_name_Other_info
    GTTCCAAGCAACTTAAGTACTGCGCATTCGCCTTGAAAACGCTTCTCGAGACTACAATTCGCTGTTGCAAGCAACAGAGA...

Your expression data must be tab delimited (if not .genes or .isoforms from a Trinity+RSEM analysis specify column for expression value):

Your blast output must follow the default tabular style (-outfmt 6):

## Running the Analysis

There are a couple of dependencies you need before going through the Venomix program (see below for installation):

* Assembled transcriptome are preferred to get good read counts using something like Trinity or Oases
* you must have a tab delimited BLAST output using ToxProt as a query against your Transcriptome, be sure to use the the -outfmt 6 option
* you must have the tab delimited output from a program like RSEM that provides expression information, such as raw count, TPM, or FPKM values (whichever you prefer)

##Necessary programs

To check for the various programs type:

    which [PROGRAM]
 
Check each individually, if you need an installed version make sure it works with your comptuer:

    which R
    which python

##Not needed for Venomix, but you will need these to get the necessary functions/files before running Venomix

    which tblastn
    which trinity #or
    which oases
    which RSEM #if not using trinity

You can download the latest [blast suite](http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download), [oases](https://www.ebi.ac.uk/~zerbino/oases/) and/or [trinity](http://trinityrnaseq.github.io/), [RSEM](http://deweylab.biostat.wisc.edu/rsem/), if you install Trinity it should be able to run RSEM within that program so this may not be necessary

## Optional Programs

signalP & Transdecoder

STILL NEED TO EXPAND ON THIS

## Program flexability

The program is designed to potentially include additional toxin genes not found in the ToxProt data set IF they share ~50% sequence similarity to what has already been described, if this is the case just append your query sequences to the original ToxProt FASTA file.

Additionally, the program could be modified to look at just about any phenotye of interst that was 

If Venomix was provide an assemblage of genes for the phenotype of interest to 

 in FASTA format, 

###Acknowledgements

This program is a direct byproduct of the training received at Friday Harbor Marine Labs whiel attending the [Practical Computing for Biologists](http://practicalcomputing.org/) workshop with [Steve Haddock](http://www.mbari.org/staff/haddock/) and [Casey Dunn](http://dunnlab.org/). The authors thank them and other members of the workshop (Aurturo Alvarez-Aguilar, Jimmy Bernot, Bill Browne, Anela Choy, Zander Fodor, Michelle Gather, Joel Kingslover, Jasmine Mah, Adelaide Rhodes, Liz Scheimer, Emily Warschefsky, Linda Wordeman, and Sara Wyckoff) for their continued support and enthusiasm for Venomix. 

Support for pursuing this project was provided by NSF to JM [NSF Award #: 1257796](http://www.nsf.gov/awardsearch/showAward?AWD_ID=1257796) and ER [NSF Award #: 1401014](http://nsf.gov/awardsearch/showAward?AWD_ID=1401014)