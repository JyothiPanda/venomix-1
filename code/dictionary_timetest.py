#!/usr/bin/env python
'''Do a comparison between speed of different 
approaches to retrieving from a dictionary

For PCfB - FHL 2015
'''

from time import time

def makeDict(total=1000):
	Klist = []
	MyDict = {}
	for Num in range(total):
		KV = "A%05d" % Num
		Klist.append(KV)
		MyDict[KV] = Num
	return MyDict

def makeFakeKeys(total=1000):
	Tlist = []
	for Num in range(total):
		KV = "A%05d" % (Num + total//4)
		Tlist.append(KV)
	return Tlist

def checkKeysInLoop(KeyList,SearchDict):
	'''Check .keys(), generated inside loop'''
	Added = 0
	for TempKey in KeyList:
		if TempKey in SearchDict.keys():
			Added = Added + SearchDict[TempKey]
	return Added

def checkKeysOutside(KeyList,SearchDict):
	'''Check .keys(), generated outside loop'''
	Added = 0
	MasterList = SearchDict.keys()
	for TempKey in KeyList:
		if TempKey in MasterList:
			Added = Added + SearchDict[TempKey]
	return Added

def tryAdding(KeyList,SearchDict):
	"Try: accessing, and pass on fail"
	Added = 0
	for TempKey in KeyList:
		try:
			Added = Added + SearchDict[TempKey]
		except KeyError:
			pass
	return Added

def useGet(KeyList,SearchDict):
	'''Use .get(), with a default value'''
	Added = 0
	for TempKey in KeyList:
		Added = Added + SearchDict.get(TempKey,0)
	return Added


def main():
	HowMany = 100000
	
	# Make dictionary with 100,000 (=HowMany) keys
	Master    = makeDict(HowMany)
	
	# Make a list of 20,000 keys, only 15,000 are present
	QueryList = makeFakeKeys(HowMany//5) 
	
	# The functions defined above can be placed in a list (!)
	# and run as a for loop...
	FunctionList = [checkKeysInLoop,
					checkKeysOutside,
					tryAdding, 
					useGet
					]
					
	print "Timing a dictionary operation for {} functions".format(len(FunctionList))
	print QueryList
	# A list to store the times of all the functions
	TList = []
	
	for Fun in FunctionList:
		Now = time()
		Added = Fun(QueryList,Master)
		Elapse = time() - Now
		TList.append(Elapse)
		print " {} took {:9.5f} secs  :  {}".format((Fun.__name__).rjust(16),Elapse,Fun.__doc__)

	# Summarize results
	print "Searched for {} keys in a dictionary of {} items.".format(HowMany//5,HowMany)
	print "The fastest was {:.1f} times faster than the slowest.".format(max(TList)/float(min(TList)))

# That name == main thingy
if __name__ == '__main__':
	main()