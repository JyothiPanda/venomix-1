#!/usr/bin/python
"""
Programing Suite: Venomix(xxx) - Main

This file retrieves information from the BLAST output and the 
ToxProt information file ().
The script first takes the UniProt table (uniref_50), but you can upload
your own tabular dataset relative to the query sequences.
together by linking "funcitonal groups" with Transcripts.

Version 0.7 (Jason Macrander)
"""

import re
import numpy
from Function import RSEMtoDict
from Function import Combine_Files
from Function import Convert_Blast
from Function import Tox_Groups
from Function import Genbank_Summary

#from Venomixxxxx import BEGIN
#from Function import Import_Genbank
#Input files provided by the users

#Blast_out_name = "../data/Blast_out_E-3"
Blast_out_name = "../data/Inputs/Blast_out_E-3" #Dynamic
Tox_Cluster = "../data/ToxProt/uniref_50.txt" #Standard
gbkFile = "../data/Inputs/sequence.gp" #Standard
RSEM_file = open("../data/Inputs/Mg-T_RSEM.genes.results", 'r') #Dynamic

Full_Trans = open("../data/Transcriptomes/Mg_Trinity.fasta", 'r') #Dynamic

#Intermediate files for your own interest/use
TPMFile = open("../data/Intermediate/Mg-T_1a.fasta", 'w')
ToxinLike = open("../data/Intermediate/Toxin_like.txt",'w')
test = 0
#BEGIN(test)

print "Inputs look good!"

"""
This function retrieves information from the RSEM file and Fasta Assembly
and writes to a file based on certain parameters.
"""


RSEM_Dict = {}

RSEM_Dict = RSEMtoDict(RSEM_file)
print "RSEM Dict Loaded!"


RSEM_Seq_Dict = Combine_Files(RSEM_Dict,Full_Trans)
print "Seq Dict Loaded!"



count = 0
for key in RSEM_Seq_Dict:
	TPM = float(RSEM_Seq_Dict.get(key).get('TPM'))
	#print TPM
	if (float(RSEM_Seq_Dict.get(key).get('TPM')) > 1.0):
		TPMFile.write(">%s\n%s\n" % (key , RSEM_Seq_Dict.get(key).get('Sequence')))
		TPM_Trans = (">%s\n%s\n" % (key , RSEM_Seq_Dict.get(key).get('Sequence')))	
		count += 1
	else:
		next
#print "There were %s sequences with a greater than 1 TPM" % (count,TPM)
print "no errors?"

print "Finished RSEM Step"
##No Item!
"""
Begin the stip which uses the Convert_Blast function to creat two dictionaries,
one with all of the ToxProt IDs as keys (TPM_Trans IDs are the eleements)and the 
other with all the TPM_Trans IDs as keys (ToxProt IDs are the eleements)
"""
print "Beginning Convert_Blast Step"
#HitD = {}
QueryD = {}
#
QueryD,HitD = Convert_Blast(Blast_out_name)
#print "Query Dictionary Size:", len(QueryD.keys())

b=Genbank_Summary(Blast_out_name, gbkFile, Tox_Cluster)
#print b
with open("/users/jmacrand/venomix/data/output", 'w') as f:
	for item in b:
		f.write(str(item)+'\n')

Groups = {}
(Groups, Genbank_Summary, Genbank_Full) = Tox_Groups(Tox_Cluster, QueryD, Blast_out_name, gbkFile)

for item in Groups:
	print "\n%s:\n%s" % (item, Groups[item])
	
	with open("../data/Inputs/files{}.txt".format(item), 'w') as file:
		for line in Groups[item]:
			line.strip('[]')
			file.write("\n{}".format(line))

		file.close()
	for key in Groups.keys():
		ToxinLike.write("\n%s:\n%s" % (item, Groups[item]))


"""
Genbank retrieval step still not done
"""
print "starting Genbank Retrieval Step"
#GenbankD = {}

# This function produces 2 files in your out directory 
#Genbank_Summary(gbkFile, Blast_out_name, Tox_Cluster, Groups)



#for item in Groups:
	#print "\n%s:\n%s" % (item, Groups[item])
	#ToxinLike.write("\n%s:\n%s" % (item, Groups[item]))
#np.save('b')
#Toxin_Out(Tox_Cluster, TPMFile)



#print GenbankD
#TranscriptomeSummary(Groups, Blast_out_name)

#Toxin_like_retrieve(Groups, )

